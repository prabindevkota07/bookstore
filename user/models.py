from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Author(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class ActivationLink(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    link = models.CharField(max_length=36)

    def __str__(self):
        return str(self.user) + ":" + self.link


class Book(models.Model):
    title = models.CharField(max_length=180)
    authors = models.ManyToManyField("Author", related_name="books")
    categories = models.ManyToManyField("Category", related_name="books")
    cover = models.ImageField(upload_to="covers")
    price = models.PositiveIntegerField()

    def __str__(self):
        authors = ", ".join([str(author) for author in self.authors.all()])
        return ", ".join((
            self.title,
            authors,
            str(self.printed_year)
        ))
