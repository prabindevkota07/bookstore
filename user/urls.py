from django.contrib.auth import views as auth_views
from . import views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import BookCreateView,  BookDeleteView, BookUpdateView, BookListView, BookDetailView


urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('home/', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('accounts/profile', BookListView.as_view(), name='list'),
    path('delete/<int:pk>', BookDeleteView.as_view(),  name='delete'),
    path('update/<int:pk>',  BookUpdateView.as_view(), name='update'),
    path('list/', BookListView.as_view(), name='list'),
    path('create/', BookCreateView.as_view(), name='create'),
    path('detail/<int:pk>', BookDetailView.as_view(), name='detail')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
