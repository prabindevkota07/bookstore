from django.shortcuts import render, redirect, reverse
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views.generic import View, FormView, RedirectView, ListView, CreateView, DetailView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Book
from .forms import BookForm


def home(request):
    return render(request, "registration/success.html", {})


def register(request):
    if request.method == 'Book':
        form = UserCreationForm(request.Book)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form': form})


class BookCreateView(LoginRequiredMixin, CreateView):
    model = Book
    form_class = BookForm
    template_name = 'create.html'
    success_url = reverse_lazy('list')


class BookListView(LoginRequiredMixin,ListView):
    model = Book
    template_name = 'list.html'


class BookUpdateView(LoginRequiredMixin,UpdateView):
    model = Book
    template_name = 'update.html'
    form_class = BookForm
    context_object_name = 'Book'
    success_url = reverse_lazy('index')


class BookDeleteView(LoginRequiredMixin,DeleteView):
    model = Book
    template_name = 'delete.html'
    context_object_name = 'Book'
    success_url = reverse_lazy('index')


class BookDetailView(LoginRequiredMixin,DetailView):
    model = Book
    template_name = 'detail.html'
    context_object_name = 'post'
